/**
 * Example ES6 module containing 2 functions which are fired in main.js
 */

export function init() {
	console.log('Successfully initiated example module');
}

export function otherFunction() {
	console.log('Successfully fired otherFunction from example module');
}