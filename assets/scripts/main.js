import * as example from './modules/example'

document.addEventListener('DOMContentLoaded', function() {

	example.init();
	example.otherFunction();

});