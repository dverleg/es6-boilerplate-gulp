Frontend project kickstarter DTC
=
##### Including [Gulp](http://gulpjs.com/), [Babelify](https://github.com/babel/babelify/) and [ES6 modules](http://www.2ality.com/2014/09/es6-modules-final.html)  
#
#
#
##### Installation  
#
###### 1. Clone repository
Navigate to your development folder (~/sites for example). 
Execute the following commands to clone the boilerplate repository:
```sh
$ git clone https://dennis_verleg@bitbucket.org/dennis_verleg/es6-boilerplate-gulp.git
$ cd es6-boilerplate-gulp
```

###### 2. Install dependencies
Install dependencies using [Yarn](https://yarnpkg.com/)
```sh
$ yarn install
```

###### 3. Run webpack
This wil run gulp dev (see package.json) which will watch your assets in the ./assets folder
```sh
$ yarn run dev
```

You can also run the following command to compile your files once without watching them
```sh
$ gulp build
```

#
#

##### Notes
```./assets/scripts/main.js``` imports all functions from ```./assets/scripts/modules/example.js```. You can remove the ```example.js``` file and all content from ```main.js```. After doing this you're all set to start creating and using your own modules.
