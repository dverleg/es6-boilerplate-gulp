'use strict';

const paths     = require('../util/paths.js');
const gulp      = require('gulp');
const svgsprite = require('gulp-svg-sprite');
const svgmin    = require('gulp-svgmin');

const spriteConfig = {
	log: 'verbose',
	svg: { 
		xmlDeclaration:  false
	},
	mode: {
		symbol: {
			dest: './',
			prefix: 'svg-%s',
			sprite: 'spritemap.svg'
		}
	}
};

gulp.task('spritemap', function() {
	return gulp.src([paths.src_svg+'*.svg'])
		.pipe(svgmin())
		.pipe(svgsprite(spriteConfig))
		.on('error', function(error){ console.log(error.message); })
		.pipe(gulp.dest(paths.dist_svg));
});
