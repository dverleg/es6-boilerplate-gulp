'use strict';

const paths     = require('../util/paths.js');
const gulp      = require('gulp');
const svgsprite = require('gulp-svg-sprite');
const svgmin    = require('gulp-svgmin');

gulp.task('svg', function() {
	return gulp.src([paths.src_svg+'*.svg'])
		.pipe(svgmin({
            plugins: [{
				collapseGroups: false
			}, {
                removeDoctype: true
            }, {
                removeComments: true
            }, {
                cleanupNumericValues: {
                    floatPrecision: 2
                }
            }, {
                convertColors: {
                    names2hex: true,
                    rgb2hex: true
                }
            }]
        }))
		.pipe(gulp.dest(paths.dist_svg));
});
