'use strict';

const gulp = require('gulp');
const runsequence = require('run-sequence');

gulp.task('dev', function() {
	runsequence('build', 'watch');
});
