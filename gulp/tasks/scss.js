'use strict';

const paths = require('../util/paths.js');
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');

gulp.task('scss', function() {
	return gulp.src([paths.src_scss+'**/*.scss', '!_'+paths.src_scss+'**/*.scss'])
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(sass({ outputStyle: 'compressed' })
		.on('error', sass.logError))
		.pipe(rename({ suffix: '.min' }))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(paths.dist_css))
});
