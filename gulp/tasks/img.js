'use strict';

const paths    = require('../util/paths.js');
const gulp     = require('gulp');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');

gulp.task('img', function() {
	return gulp.src([paths.src_img+'**/*.{png,jpg,jpeg,gif,ico,xml,json,svg}'])
		.pipe(imagemin({
			progressive: true,
			use: [pngquant()]
		}))
		.pipe(gulp.dest(paths.dist_img));
});
