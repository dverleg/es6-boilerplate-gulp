'use strict';

const paths = require('../util/paths.js');
const gulp  = require('gulp');

gulp.task('watch', function() {
	gulp.watch(`${paths.src_scss}**/*.scss`, ['scss']);
	gulp.watch(`${paths.src_js}**/*.js`, ['js']);
	gulp.watch([paths.src_svg+'**/*.svg'], ['svg', 'spritemap']);
});
