'use strict';

const paths = require('../util/paths.js');
const gulp = require('gulp');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');

gulp.task('js', function() {
	return browserify("./assets/scripts/main.js")
		.transform("babelify", {
			presets: ["es2015"],
			minified: true
		})
		.bundle()
		.pipe(source('main.min.js'))
		.pipe(gulp.dest(paths.dist_js));
});
