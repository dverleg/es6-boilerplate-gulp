'use strict';

const gulp = require('gulp');
const runsequence = require('run-sequence');

gulp.task('build', function() {
	runsequence('clean', 'js', 'scss', 'img', 'svg', 'spritemap');
});
