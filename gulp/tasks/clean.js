'use strict';

const paths = require('../util/paths.js');
const gulp  = require('gulp');
const clean = require('gulp-clean');

gulp.task('clean', function () {
	return gulp.src([paths.dist], { read: false })
		.pipe(clean({ force: true }));
});
