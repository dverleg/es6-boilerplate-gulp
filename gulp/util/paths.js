module.exports = {
	src: "./assets/",
	src_js: "./assets/scripts/",
	src_scss: "./assets/styles/",
	src_img: "./assets/images/",
	src_svg: "./assets/sprites/",

	dist: "./public/assets/",
	dist_js: "./public/assets/js/",
	dist_css: "./public/assets/css/",
	dist_img: "./public/assets/img/",
	dist_svg: "./public/assets/svg/",
}
