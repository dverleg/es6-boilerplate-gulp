var fs = require('fs');
var scripts = require('./gulp/util/scriptFilter');
var tasks = fs.readdirSync('./gulp/tasks/').filter(scripts);

tasks.forEach(function(task) {
	require('./gulp/tasks/'+task);
});
